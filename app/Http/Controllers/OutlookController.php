<?php

namespace App\Http\Controllers;

use App\Classes\OutlookService;
use App\Http\Requests\GetTokenRequest;
use App\User;
use App\OutlookEvent;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class OutlookController extends Controller
{

    public function signin()
    {
        // Initialize the OAuth client
        $oauthClient = OutlookService::oauthClient();
        $authorizationUrl = $oauthClient->getAuthorizationUrl();
        //OutlookService::saveOauthState($oauthClient->getState());

        // Redirect to authorization endpoint
        return redirect($authorizationUrl);
    }

    // Authorization
    public function getToken(GetTokenRequest $request)
    {
        $user = User::find(1);

        // Initialize the OAuth client
        $oauthClient = OutlookService::oauthClient();

        // Make the token request
        $accessToken = $oauthClient->getAccessToken('authorization_code', [
            'code' => $request->input('code')
        ]);

        OutlookService::storeTokens($accessToken->getToken(), $accessToken->getRefreshToken(), $accessToken->getExpires());

        // There should be redirection
        return $user->outlookToken->access_token;
    }


    public function getFolderEmails($folder)
    {
        //$getEmailsUrl = '/me/mailfolders/'.$folder.'/messages?'.http_build_query(OutlookService::$emailQueryParams);
        $getEmailsUrl = '/me/mailfolders/'.$folder.'/messages';

        return OutlookService::makeRequest('GET', $getEmailsUrl, Model\Message::class);
    }


    public function inbox()
    {
        return $this->getFolderEmails('Inbox');
    }


    public function sendEmail(Request $request)
    {
        return OutlookService::makeRequest('POST', '/me/sendmail', Model\Message::class, $request->all());
    }


    public function folderList()
    {
        return OutlookService::makeRequest('GET', '/me/MailFolders', Model\MailFolder::class);
    }

    public function addFolder(Request $request)
    {
        return OutlookService::makeRequest('POST', '/me/MailFolders', Model\Message::class, $request->all());
    }

    public function getMessage($message)
    {
        return OutlookService::makeRequest('GET', '/me/messages/'.$message, Model\Message::class);
    }


    public function markReadUnread($message)
    {
        $newState = !$this->getMessage($message)->getIsRead();
        $json_data = json_encode(['isRead' => $newState]);

        return OutlookService::makeRequest('PATCH', '/me/messages/'.$message, Model\Message::class, $json_data);
    }



    // Calendar
    public function getAllCalendars()
    {
        return OutlookService::makeRequest('GET', '/me/calendars', Model\Event::class);
    }


    public function getCalendarEvents(Request $request, $calendar)
    {
        $getEventsUrl = '/me/calendars/'.$calendar.
                        '/calendarview?startDateTime='.$request->input('startDateTime').
                        '&endDateTime='.$request->input('endDateTime');
        //?startDateTime=2018-06-01T01:00:00&endDateTime=2018-07-31T23:00:00

        return OutlookService::makeRequest('GET', $getEventsUrl, Model\Event::class);
    }


    public function getCalendarEventsFromDB(Request $request, $calendar)
    {
        $service = new OutlookService;
        return $service->eventsFromDB($request, $calendar, $this->getCalendarEvents($request, $calendar));
    }


    public function addCalendarEvent(Request $request, $calendar)
    {
        return $this->addEvent($request, $calendar);
    }


    public function addCalendarEventDB(Request $request, $calendar)
    {
        return $this->addEvent($request, $calendar, true);
    }

    public function addEvent($request, $calendar, $database = false)
    {
        $addEvent = OutlookService::makeRequest('POST', '/me/calendars/'.$calendar.'/events', Model\Event::class, $request->all());

        if($database) {
            OutlookEvent::create([
                'event_id' => $addEvent->getId(),
                'calendar_id' => $calendar,
                'user_id' => 1
            ]);
        }

        return $addEvent;
    }

    public function updateEvent(Request $request, $event)
    {
        return OutlookService::makeRequest('PATCH', '/me/events/'.$event, Model\Event::class, $request->all());
    }


    public function deleteEvent($event)
    {
        return OutlookService::makeRequest('DELETE', '/me/events/'.$event, Model\Event::class);
    }
}
