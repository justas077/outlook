<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutlookEvent extends Model
{
    protected $fillable = ['event_id', 'calendar_id', 'user_id'];
}
