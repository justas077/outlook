<?php

namespace App\Classes;

use App\OutlookEvent;
use App\OutlookToken;
use App\User;
use http\Env\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Illuminate\Support\Facades\DB;

class OutlookService
{
    public static $emailQueryParams = array (
        // Only return Subject, ReceivedDateTime, and From fields
        "\$select" => "subject,receivedDateTime,from",
        // Sort by ReceivedDateTime, newest first
        "\$orderby" => "receivedDateTime ASC"
    );

    public static $eventsQueryParams = array (
        // // Only return Subject, Start, and End fields
        "\$select" => "subject,start,end",
        // Sort by Start, oldest first
        "\$orderby" => "Start/DateTime"
    );

    public static function oauthClient()
    {
        return new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => env('OAUTH_APP_ID'),
            'clientSecret'            => env('OAUTH_APP_PASSWORD'),
            'redirectUri'             => env('OAUTH_REDIRECT_URI'),
            'urlAuthorize'            => env('OAUTH_AUTHORITY').env('OAUTH_AUTHORIZE_ENDPOINT'),
            'urlAccessToken'          => env('OAUTH_AUTHORITY').env('OAUTH_TOKEN_ENDPOINT'),
            'urlResourceOwnerDetails' => '',
            'scopes'                  => env('OAUTH_SCOPES')
        ]);
    }

    public static function saveOauthState($state)
    {
        $user = User::find(1);

        $tokenTable = OutlookToken::find($user)->first();
        //$tokenTable->oauth_state = $state;
        $tokenTable->token_expires = 0;
        $tokenTable->save();
    }

    public static function storeTokens($access_token, $refresh_token, $expires)
    {
        $user = User::find(1);

        $tokenTable = OutlookToken::find($user)->first();
        $tokenTable->access_token = $access_token;
        $tokenTable->refresh_token = $refresh_token;
        $tokenTable->token_expires = $expires;
        $tokenTable->save();
    }

    public function getAccessToken()
    {
        $user = User::find(1);

        // Check if token is expired
        $now = time() + 300;
        if ($user->outlookToken->token_expires <= $now) {
            // Token is expired

            $oauthClient = $this->oauthClient();
            $newToken = $oauthClient->getAccessToken('refresh_token', [
                'refresh_token' => $user->outlookToken->refresh_token
            ]);

            // Store refresh token
            $this->storeTokens($newToken->getToken(), $newToken->getRefreshToken(), $newToken->getExpires());
            return $newToken->getToken();
        }
        else {
            // Token is still valid
            return $user->outlookToken->access_token;
        }
    }

    public static function makeRequest($requestType, $url, $returnType, $requestData = null)
    {
        $outlookService = new OutlookService;

        $graph = new Graph();
        $graph->setAccessToken($outlookService->getAccessToken());

        $data = $graph  ->createRequest($requestType, $url)
                        ->setReturnType($returnType);

        if($requestType == 'POST' or $requestType == 'PATCH') {
            $data->attachBody($requestData);
        }

        return $data->execute();
    }

    public function createIdArray($data, $outlookObject = false)
    {
        $array = [];

        foreach ($data as $element) {
            if($outlookObject) {
                $element = $element->getId();
            } else {  // Database object
                $element = $element->event_id;
            }
            array_push($array, $element);
        }

        return $array;
    }

    public function eventsFromDB($request, $calendarId, $outlookEvents)
    {
        $user = User::find(1);
        $databaseEvents = $user->events ->where('calendar_id', $calendarId)
                                        ->where('created_at', '>=', $request->input('startDateTime'))
                                        ->where('created_at', '<=', $request->input('endDateTime'));

        $this->matchingOutlookWithDatabase($calendarId, $databaseEvents, $outlookEvents);


        $databaseEventsWithOutlookData = [];

        foreach ($outlookEvents as $event) {
            if(in_array($event->getId(), $this->createIdArray($databaseEvents))) {      // Search events with same id
                array_push($databaseEventsWithOutlookData, $event);
            }
        }

        return $databaseEventsWithOutlookData;
    }

    public function matchingOutlookWithDatabase($calendar, $databaseEvents, $outlookEvents)
    {
        DB::beginTransaction();

        foreach ($databaseEvents as $event) {
            if(!in_array($event->event_id, $this->createIdArray($outlookEvents, true))) {
                $data = [
                    "Subject" => "There should be topic",
                    "Body" => [
                        "ContentType" => "HTML",
                        "Content" => "There should be content"
                    ],
                    "Start" => [
                        "DateTime" => "2018-06-15 13:20:39",
                        "TimeZone" => "Europe/Vilnius"
                    ],
                    "End" => [
                        "DateTime" => "2018-06-15 16:20:39",
                        "TimeZone" => "Europe/Vilnius"
                    ]
                ];

                try {
                    $createEvent = OutlookService::makeRequest('POST', '/me/calendars/'.$calendar.'/events', Model\Event::class, $data);
                    $eventTable = OutlookEvent::find($event->id);
                    $eventTable->event_id = $createEvent->getId();
                    $eventTable->save();

                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                }
            }
        }
    }
}