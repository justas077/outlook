<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/signin', 'OutlookController@signin');
Route::get('/authorize', 'OutlookController@getToken');

// Email
Route::get('/inbox', 'OutlookController@inbox');
Route::get('/folderList', 'OutlookController@folderList');
Route::get('/folder/{folder}', 'OutlookController@getFolderEmails');
Route::get('/getEmail/{email}', 'OutlookController@getMessage');
Route::post('/addFolder', 'OutlookController@addFolder');
Route::post('/sendEmail', 'OutlookController@sendEmail');
Route::patch('/markReadUnread/{message}', 'OutlookController@markReadUnread');


// Calendar
Route::get('/allCalendars', 'OutlookController@getAllCalendars');
Route::get('/calendar/{calendar}', 'OutlookController@getCalendarEvents');
Route::get('/calendarDB/{calendar}', 'OutlookController@getCalendarEventsFromDB');
Route::post('/addEvent/{calendar}', 'OutlookController@addCalendarEvent');
Route::post('/addEventDB/{calendar}', 'OutlookController@addCalendarEventDB');
Route::delete('/deleteEvent/{event}', 'OutlookController@deleteEvent');
Route::patch('/updateEvent/{event}', 'OutlookController@updateEvent');