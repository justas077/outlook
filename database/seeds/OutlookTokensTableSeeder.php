<?php

use Illuminate\Database\Seeder;

class OutlookTokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('outlook_tokens')->insert([
            'token_expires' => 1,
            'user_id' => 1
        ]);
    }
}
