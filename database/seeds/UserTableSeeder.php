<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'justas',
            'email' => 'justas077@gmail.com',
            'password' => '$2y$10$uBg/wi1YmwftYQYTozry4Oz6o/Tr837U10JL8jqxDlMyewFeWfv.W' //123456
        ]);
    }
}
