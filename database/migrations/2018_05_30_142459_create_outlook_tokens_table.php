<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutlookTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlook_tokens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('oauth_state')->nullable($value = true);
            $table->mediumText('access_token')->nullable($value = true);
            $table->mediumText('refresh_token')->nullable($value = true);
            $table->integer('token_expires')->nullable($value = true);
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outlook_tokens');
    }
}
